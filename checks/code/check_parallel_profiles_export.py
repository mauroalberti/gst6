
import sys
import os

from gst.io.vectors.ogr_io import try_read_line_shp_with_attr, try_create_write_line_shapefile
from pygsf.profiles.profilers import LineProfiler, Profiler

src_profile_shapefile_pth = "/home/mauro/Documents/GeoDati/italia/macrozone/merid/regioni/basilicata/mt_alpi/livelli/profile_two.shp"
shapefile_name = "profiles10.shp"
folder_pth = "/home/mauro/Documents/projects/gsf/docs/notebooks/"


success, result = try_read_line_shp_with_attr(src_profile_shapefile_pth)

if not success:
    sys.exit(0)

[geoline], _ = result[0]

projected_geoline = geoline.project(
        dest_epsg_cd=32633
    )

line = projected_geoline.shape

line_profiler = LineProfiler(
    src_line=line,
    epsg_code=32633)

pp = Profiler(
    base_profiler=line_profiler,
    num_profiles=21,
    offset=10, # meters
)

line_profiler = LineProfiler(
    src_line=line,
    epsg_code=32633)

pp = Profiler(
    base_profiler=line_profiler,
    num_profiles=21,
    offset=10,  # meters
)

records_values = []
for ndx, profiler in enumerate(pp):
    points_coords = profiler.line.coords()
    attributes = [ndx]
    records_values.append((points_coords, attributes))

for rv in records_values:
    print(rv)

output_shapefile_path = os.path.join(folder_pth, shapefile_name)
print(f"creating output shapefile {output_shapefile_path}")

success, result = try_create_write_line_shapefile(
    shapefile_path=os.path.join(folder_pth, shapefile_name),
    fields_dict_list=[{"name": "cod", "ogr_type": 'ogr.OFTInteger'}],
    records_values=records_values,
    epsg_code=pp.epsg_code
)

print(success)
print(result)



