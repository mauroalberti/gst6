
from PyQt6.QtGui import QColor

from pygsf.inspections.errors import *


def qcolor2rgbmpl(
        qcolor
):

    red = qcolor.red() / 255.0
    green = qcolor.green() / 255.0
    blue = qcolor.blue() / 255.0
    return red, green, blue


def fract_rgb_from_qcolor_name(color_name):
    """
    return tuple of three float values [0,1]
    """

    color = QColor(color_name)

    red = color.red() / 255.0
    green = color.green() / 255.0
    blue = color.blue() / 255.0

    return red, green, blue
