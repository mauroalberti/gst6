
from PyQt6 import QtWidgets

import matplotlib

# Make sure that we are using QT6
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


class ScrollableWindow(QtWidgets.QDialog):
    """
    from:
    https://stackoverflow.com/questions/42622146/scrollbar-on-matplotlib-showing-page
    """
    def __init__(self,
                 fig,
                 title,
                 parent=None
                 ):

        super(ScrollableWindow, self).__init__(parent)

        self.setLayout(QtWidgets.QVBoxLayout())

        self.fig = fig
        self.canvas = FigureCanvas(self.fig)
        self.canvas.draw()
        self.scroll = QtWidgets.QScrollArea(self)
        self.scroll.setWidget(self.canvas)

        self.nav = NavigationToolbar(self.canvas, self)
        self.layout().addWidget(self.nav)
        self.layout().addWidget(self.scroll)

        self.setWindowTitle(title)

        self.show()


