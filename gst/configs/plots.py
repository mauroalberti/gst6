
# General
color_def = 'grey'
aspect_def = 1
superposed_def = False

# Attitudes
a_color_def = "red"
a_labels_orientions_def = True
a_labels_ids_def = True

# Line intersections
li_color_def = "red"
li_size_def = 3
li_alpha_def = 0.5
li_labels_def = True

# Polygons intersections
pi_linewidth = 2
pi_labels_def = True
pi_legend_def = True
