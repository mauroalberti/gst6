
import numbers
import traceback
from typing import Dict

import numpy as np

try:
    from osgeo import gdal
except ImportError:
    import gdal

try:
    from osgeo import osr
except ImportError:
    import osr

from pygsf.inspections.errors import *
from pygsf.geometries.grids.rasters import *


def extract_raster_params(
    file_ref: Any
) -> Tuple[
    Union[type(None), Tuple[gdal.Dataset, Union[type(None), GeoTransform], numbers.Integral, numbers.Integral, 'gdal.Projection']], Error]:
    """
    Read raster parameters.

    :param file_ref: the reference to the raster
    :return: the dataset, the parsed geotransform, the number of bands, the parsed projection,
              the original geotransform, the original projection as a tuple, plus the error status.
    """

    try:

        # open raster file and check operation success

        dataset = gdal.Open(file_ref, gdal.GA_ReadOnly)

        if not dataset:
            return None, Error(
                True,
                caller_name(),
                Exception("No input data open"),
                traceback.format_exc()
            )

        # get raster descriptive infos

        gdal_geotransform = dataset.GetGeoTransform()

        if gdal_geotransform:
            geotransform = GeoTransform.from_gdal_geotransform(gdal_geotransform)
        else:
            geotransform = None

        num_bands = dataset.RasterCount

        # https://gis.stackexchange.com/questions/267321/extracting-epsg-from-a-raster-using-gdal-bindings-in-python
        # does not work -> epsg = int(gdal.Info(input, format='json')['coordinateSystem']['wkt'].rsplit('"EPSG","', 1)[-1].split('"')[0])

        gdal_projection = osr.SpatialReference(wkt=dataset.GetProjection())
        gdal_projection.AutoIdentifyEPSG()

        try:
            epsg = int(gdal_projection.GetAttrValue('AUTHORITY', 1))
        except:
            epsg = -1

        return (dataset, geotransform, num_bands, epsg, gdal_projection), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def decompose_raster(
        file_ref: Any
) -> Tuple[Union[type(None), Tuple[gdal.Dataset, Union[type(None), GeoTransform], numbers.Integral, numbers.Integral]], Error]:
    """
    Read a raster layer.

    :param file_ref: the reference to the raster
    :return: the dataset, its geotransform, the number of bands, the projection as a tuple, plus the error status.
    """

    try:

        # open raster file and check operation success

        dataset = gdal.Open(file_ref, gdal.GA_ReadOnly)

        if not dataset:
            return None, Error(
                True,
                caller_name(),
                Exception("No input data open"),
                traceback.format_exc()
            )

        # get raster descriptive infos

        gt = dataset.GetGeoTransform()

        if gt:
            geotransform = GeoTransform.from_gdal_geotransform(gt)
        else:
            geotransform = None

        num_bands = dataset.RasterCount

        # https://gis.stackexchange.com/questions/267321/extracting-epsg-from-a-raster-using-gdal-bindings-in-python
        # does not work -> epsg = int(gdal.Info(input, format='json')['coordinateSystem']['wkt'].rsplit('"EPSG","', 1)[-1].split('"')[0])

        proj = osr.SpatialReference(wkt=dataset.GetProjection())
        proj.AutoIdentifyEPSG()

        try:
            epsg = int(proj.GetAttrValue('AUTHORITY', 1))
        except:
            epsg = -1

        return (dataset, geotransform, num_bands, epsg), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def raster_band(
        dataset: gdal.Dataset,
        bnd_ndx: int = 1
) -> Tuple[type(None), Tuple[dict, 'np.array'], Error]:
    """
    Read data and metadata of a rasters band based on GDAL.

    :param dataset: the source raster dataset
    :param bnd_ndx: the index of the band (starts from 1)
    :return: the band parameters and the data values
    """

    try:

        band = dataset.GetRasterBand(bnd_ndx)
        data_type = gdal.GetDataTypeName(band.DataType)

        unit_type = band.GetUnitType()

        stats = band.GetStatistics(False, False)

        if stats is None:
            dStats = dict(
                min=None,
                max=None,
                mean=None,
                std_dev=None)
        else:
            dStats = dict(
                min=stats[0],
                max=stats[1],
                mean=stats[2],
                std_dev=stats[3])

        noDataVal = band.GetNoDataValue()

        nOverviews = band.GetOverviewCount()

        colorTable = band.GetRasterColorTable()

        if colorTable:
            nColTableEntries = colorTable.GetCount()
        else:
            nColTableEntries = 0

        # read data from band

        grid_values = band.ReadAsArray()
        if grid_values is None:
            raise Exception("Unable to read data from rasters")

        # transform data into numpy array

        data = np.asarray(grid_values)

        # if nodatavalue exists, set null values to NaN in numpy array
        if noDataVal is not None and np.isfinite(noDataVal):
            data = np.where(abs(data - noDataVal) > 1e-10, data, np.NaN)

        band_params = dict(
            dataType=data_type,
            unitType=unit_type,
            stats=dStats,
            noData=noDataVal,
            numOverviews=nOverviews,
            numColorTableEntries=nColTableEntries)

        return (band_params, data), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_raster_band(
    raster_source: str,
    bnd_ndx: int = 1
) -> Tuple[Union[type(None), Tuple[GeoTransform, numbers.Integral, Dict, 'np.array']], Error]:
    """
    Read the band of a raster.
    Implicit band index is 1 (the first band).

    :param raster_source: the raster path.
    :param bnd_ndx: the band index. Implicit value is 1 (first band).
    :return: a tuple of results, plus the error status.
    """

    try:

        result, err = decompose_raster(raster_source)
        if err:
            return None, err

        dataset, geotransform, num_bands, epsg = result

        result, err = raster_band(dataset, bnd_ndx)
        if err:
            return None, err

        band_params, data = result

        return (geotransform, epsg, band_params, data), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_raster_band_with_projection(
    raster_source: str,
    bnd_ndx: int = 1
) -> Tuple[Union[type(None), Tuple[GeoTransform, numbers.Integral, Dict, np.ndarray, 'gdal.Projection']], Error]:
    """
    Read the band of a raster.
    Implicit band index is 1 (the first band).

    :param raster_source: the raster path.
    :param bnd_ndx: the band index. Implicit value is 1 (first band).
    :return: a tuple of results, plus the error status.
    """

    try:

        result, err = extract_raster_params(raster_source)
        if err:
            return None, err

        dataset, geotransform, num_bands, epsg, gdal_projection = result

        result, err = raster_band(dataset, bnd_ndx)
        if err:
            return None, err

        band_params, data = result

        return (geotransform, epsg, band_params, data, gdal_projection), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def write_geotiff(
    file_path: str,
    arr_out: np.ndarray,
    geotransform: 'gdal.GeoTransform',
    projection: 'gdal.Projection',
) -> Error:
    """

    Modified from:
    https://gis.stackexchange.com/questions/164853/reading-modifying-and-writing-a-geotiff-with-gdal-in-python
    Answer by Andrea Massetti
    Consulted on 2022-10-16.

    :return: Error
    """

    try:

        driver = gdal.GetDriverByName("GTiff")

        [rows, cols] = arr_out.shape

        outdata = driver.Create(
            file_path,
            cols,
            rows,
            1,
            gdal.GDT_Float64
        )

        outdata.SetGeoTransform(geotransform)
        outdata.SetProjection(projection.ExportToWkt())
        outdata.GetRasterBand(1).WriteArray(arr_out)
        outdata.FlushCache()

        return Error()

    except Exception as e:

        return Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


if __name__ == "__main__":

    import doctest
    doctest.testmod()

