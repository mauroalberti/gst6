
from matplotlib.figure import Figure

from pygsf.inspections.errors import *

from pygsf.geometries.lines import *


def plot_line(
    fig: Figure,
    line: Ln
) -> Figure:
    """
    Plot a line.

    :param fig: the figure in which to plot the line.
    :param line: the line to plot.
    :return: the input Figure instance.
    """

    fig.gca().plot(line.x_list(), line.y_list(), '-')

    return fig


def plot_lines(
    fig: Figure,
    lines: List[Ln],
    color: str = "blue",
    linestyle: str = '-',
    linewidth: numbers.Real = 0.5,
    labels: bool = True,
    **kargs
) -> Figure:

    ax = fig.gca()

    for ndx, line in enumerate(lines):

        if labels and (ndx+1) % 5 == 0:
           line_color = 'red'
           line_width = linewidth * 2
        else:
            line_color = color
            line_width = linewidth

        ax.plot(
            line.x_list(),
            line.y_list(),
            color=line_color,
            linestyle=linestyle,
            linewidth=line_width,
            **kargs)

        if labels and (ndx+1) % 5 == 0:
            end_point = line[-1].end_pt
            ax.text(end_point.x, end_point.y, f'{ndx+1}')

    return fig

