# gst6



## What is

**gst** is a Python module for geospatial data handling.
The name means **G**eo**S**patial**T**ools and the final **6** refers to Qt6, that is used
for windowing.

It is mainly devoted to data io and conversion, relying on other Python geospatial modules. 

It derives from **gst**, that still uses Qt5.

## License
GPL-3

## Project status
Alpha